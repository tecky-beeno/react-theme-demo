import React from 'react'
import { useSelector } from 'react-redux'
import { State } from '../redux/store'
import HomePage from './HomePage'

export function Main() {
  const mainTheme = useSelector((state: State) => state.theme.main)
  return (
    <main style={mainTheme}>
      main
      {/* Omit Router for simple demo */}
      <HomePage />
    </main>
  )
}

export default Main
