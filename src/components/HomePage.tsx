import { useSelector } from 'react-redux'
import logo from '../logo.svg'
import { State } from '../redux/store'
import homeStyles from './HomePage.module.css'

export function HomePage() {
  const homeTheme = useSelector((state: State) => state.theme.home)
  return (
    <div>
      <p style={homeTheme.p}>Welcome to the home page</p>
      <div>
        <img
          src={logo}
          className={homeStyles.avatar}
          alt="sample user avatar"
        />
      </div>
    </div>
  )
}

export default HomePage
