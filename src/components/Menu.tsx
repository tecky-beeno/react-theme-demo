import React from 'react'
import { useDispatch } from 'react-redux'
import { ChangeTheme, ThemeName, ThemeNames } from '../redux/theme'

export function Menu() {
  const dispatch = useDispatch()

  function setTheme(theme: ThemeName) {
    dispatch(ChangeTheme(theme))
  }

  return (
    <div>
      {ThemeNames.map(theme => (
        <button onClick={() => setTheme(theme)} key={theme}>
          {theme}
        </button>
      ))}
    </div>
  )
}

export default Menu
