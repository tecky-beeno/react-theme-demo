export type Theme = {
  main: {
    color: string // for text
    backgroundColor: string
  }
  home: {
    p: {
      color: string
    }
  }
}

export const LightTheme: Theme = {
  main: {
    color: 'black',
    backgroundColor: 'white',
  },
  home: {
    p: {
      color: 'darkgreen',
    },
  },
}

export const DarkTheme: Theme = {
  main: {
    color: 'white',
    backgroundColor: 'black',
  },
  home: {
    p: {
      color: 'lightgreen',
    },
  },
}

export const Themes = {
  LightTheme,
  DarkTheme,
}

export type ThemeName = keyof typeof Themes

export const ThemeNames = Object.keys(Themes) as ThemeName[]

export function DefaultTheme(): Theme {
  let themeName = localStorage.getItem('theme') as ThemeName
  return Themes[themeName] || LightTheme
}

export function ChangeTheme(themeName: ThemeName) {
  return {
    type: '@@Theme/change' as const,
    themeName,
  }
}

export type ThemeAction = ReturnType<typeof ChangeTheme>

export const ThemeReducer = (
  state: Theme = DefaultTheme(),
  action: ThemeAction,
): Theme => {
  switch (action.type) {
    case '@@Theme/change':
      localStorage.setItem('theme', action.themeName)
      return Themes[action.themeName] || state
    default:
      return state
  }
}

export default ThemeReducer
