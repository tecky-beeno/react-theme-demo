import { createStore, combineReducers } from 'redux'
import theme from './theme'

export const reducer = combineReducers({
  theme,
})

export type State = ReturnType<typeof reducer>

export const store = createStore(reducer)

export default store
