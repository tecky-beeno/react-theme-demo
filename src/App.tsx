import React from 'react'
import { Provider } from 'react-redux'
import appStyles from './App.module.css'
import store from './redux/store'
import Menu from './components/Menu'
import Main from './components/Main'

export function App() {
  return (
    <Provider store={store}>
      <div className={appStyles.App}>
        <Menu />
        <Main />
      </div>
    </Provider>
  )
}

export default App
